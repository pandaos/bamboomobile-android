package com.pandaos.bamboomobile.ui;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pandaos.bamboomobile.R;
import com.pandaos.pvpclient.objects.PvpEntry;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Oren Kosto on 5/4/15.
 */
@EViewGroup(R.layout.activity_main_entry_list_item)
public class EntryListItemView extends RelativeLayout {

    private Context context;

    @ViewById
    ImageView main_activity_entry_list_item_image;

    @ViewById
    TextView main_activity_entry_list_item_name;

    public EntryListItemView(Context context) {
        super(context);
        this.context = context;
    }

    public void bind(PvpEntry entry) {
        main_activity_entry_list_item_name.setText(entry.name);

        Picasso.with(context)
                .load(entry.thumbnailUrl + "/height/" + this.getHeight() + "/width/" + this.getWidth() + "type/3")
                .fit()
                .into(main_activity_entry_list_item_image);
    }
}
