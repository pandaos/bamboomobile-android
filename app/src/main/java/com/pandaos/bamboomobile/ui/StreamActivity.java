package com.pandaos.bamboomobile.ui;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.pandaos.bamboomobile.R;
import com.pandaos.bamboomobile.util.BambooMobile;
import com.pandaos.bamboomobile.util.CameraView;
import com.pandaos.bamboomobile.util.PvpStreamModel;
import com.pandaos.pvpclient.objects.PvpLiveEntry;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_stream)
public class StreamActivity extends Activity {

    @App
    BambooMobile application;

    @Bean
    PvpStreamModel pvpStreamModel;

    @ViewById(R.id.recorder_control)
    Button recordButton;

    @ViewById(R.id.camera_view)
    RelativeLayout cameraContainer;

    /* video data getting thread */
    Camera cameraDevice;
    int cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
    CameraView cameraView;
    Boolean isRecording = false;
    PvpLiveEntry liveEntry;
    final int STREAM_PERMISSIONS_REQUEST_CODE = 1;
    final String[] STREAM_PERMISSIONS = new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    boolean streamPermissionsGranted = false;

    @AfterViews
    void afterViews() {
        //Video properties
        PvpStreamModel.sampleAudioRateInHz = 44100;
        PvpStreamModel.imageWidth = 640;
        PvpStreamModel.imageHeight = 480;
        PvpStreamModel.frameRate = 30;

        PvpStreamModel.ffmpeg_link = getIntent().getStringExtra("streamUrl");

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, STREAM_PERMISSIONS, STREAM_PERMISSIONS_REQUEST_CODE);
        } else {
            //Init camera preview layout
            streamPermissionsGranted = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (streamPermissionsGranted) {
            initCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isRecording) {
            recorder_control();
        }
    }

    @Click
    void recorder_control() {
        if(isRecording) {
            Log.i("TAG", "stop recording");
            pvpStreamModel.stopRecording();
            isRecording = false;
            recordButton.setText("Start Recording");
        } else {
            Log.i("TAG", "start recording");
            pvpStreamModel.startRecording();
            isRecording = true;
            recordButton.setText("Stop Recording");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case STREAM_PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length == STREAM_PERMISSIONS.length) {
                    for (int i = 0; i < STREAM_PERMISSIONS.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            break;
                        }
                    }
                    streamPermissionsGranted = true;
                }
            }
            break;
        }
    }

    /**
     * Init activity layout
     */
    private void initCamera() {
        cameraContainer.removeAllViews();

        //Get camera device
        cameraView = new CameraView(this, cameraId);

        cameraContainer.addView(cameraView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
