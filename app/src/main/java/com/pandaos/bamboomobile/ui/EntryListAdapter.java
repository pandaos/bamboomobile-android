package com.pandaos.bamboomobile.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pandaos.bamboomobile.util.BambooMobile;
import com.pandaos.pvpclient.models.SharedPreferences_;
import com.pandaos.pvpclient.objects.PvpEntry;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oren Kosto on 5/4/15.
 */
@EBean
public class EntryListAdapter extends BaseAdapter {
    public List<PvpEntry> items = new ArrayList<>();

    @App
    BambooMobile application;

    @Pref
    SharedPreferences_ prefs;

    @RootContext
    Context context;

    @AfterInject
    void initAdapter() {
//        items = application.entries;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        initAdapter();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public PvpEntry getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EntryListItemView listItemView = convertView == null
                ? EntryListItemView_.build(context)
                : (EntryListItemView) convertView;
        listItemView.bind(getItem(position));
        return listItemView;
    }
}
