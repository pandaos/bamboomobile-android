package com.pandaos.bamboomobile.ui;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pandaos.bamboomobile.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Oren Kosto on 5/2/15.
 */
@EViewGroup(R.layout.activity_main_drawer_list_item)
public class CategoryListItemView extends RelativeLayout {
    @ViewById
    TextView main_activity_drawer_list_item_text;

    public CategoryListItemView(Context context) {
        super(context);
    }

    public void bind(String name) {
        main_activity_drawer_list_item_text.setText(name);
    }
}
