package com.pandaos.bamboomobile.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.pandaos.bamboomobile.R;
import com.pandaos.bamboomobile.util.BambooMobile;
import com.pandaos.bamboomobile.util.Constants;
import com.pandaos.pvpclient.models.PvpConfigModel;
import com.pandaos.pvpclient.models.PvpConfigModelCallback;
import com.pandaos.pvpclient.services.PvpRestService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

@EActivity(R.layout.activity_splash_screen)
public class SplashScreenActivity extends Activity implements PvpConfigModelCallback {

//    private SystemUiHider mSystemUiHider;
    private int configRetryCount = 0;
    private AlertDialog.Builder errorDialog;

    @App
    BambooMobile application;

    @ViewById
    RelativeLayout splash_screen_main_view;

    @ViewById
    ProgressBar splash_screen_progress_view;

    @Bean
    PvpConfigModel configModel;

//    @RestService
//    PvpRestService restService;

    @AfterViews
    void afterViews() {
//        restService.setRootUrl("");
        errorDialog = new AlertDialog.Builder(this)
                .setTitle("Server Error")
                .setMessage("There was a problem reaching our servers, Please try again later.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        killApp();
                    }
                });

        showProgressBar(Constants.SYSTEM_UI_AUTO_HIDE_DELAY_MILLIS);
        getConfigDelayed();
    }

    @Background
    void getConfig() {
        configModel.getConfig(this);
    }

    @Background(delay = Constants.SYSTEM_UI_AUTO_HIDE_DELAY_MILLIS)
    void getConfigDelayed() {
        getConfig();
    }

    @Override
    public void configRequestSuccess() {
        System.out.println("Got config, entering application...");
        Intent intent = new Intent(this, MainActivity_.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void configRequestFail() {
        configRetryCount++;
        if (configRetryCount < 5) {
            System.out.println("Failed to retrieve server config, performing retry " + configRetryCount);
            getConfig();
        } else {
            System.out.println("Failed to retrieve server config, exiting...");
            showErrorDialog();
        }
    }

    Handler showProgressBarHandler = new Handler();
    Runnable showProgressBarRunnable = new Runnable() {
        @Override
        public void run() {
            splash_screen_progress_view.setVisibility(View.VISIBLE);
        }
    };

    @UiThread
    void showProgressBar(int delayMillis) {
        showProgressBarHandler.removeCallbacks(showProgressBarRunnable);
        showProgressBarHandler.postDelayed(showProgressBarRunnable, delayMillis);
    }

    @UiThread
    void showErrorDialog() {
        errorDialog.show();
    }

    @Background(delay = 1000)
    void killApp() {
        System.exit(0);
    }
}
