package com.pandaos.bamboomobile.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.pandaos.bamboomobile.R;
import com.pandaos.bamboomobile.util.BambooMobile;
import com.pandaos.bamboomobile.util.Constants;
import com.pandaos.pvpclient.models.PvpEntryModel;
import com.pandaos.pvpclient.models.PvpEntryModelCallback;
import com.pandaos.pvpclient.models.PvpLiveEntryModel;
import com.pandaos.pvpclient.models.PvpLiveEntryModelCallback;
import com.pandaos.pvpclient.models.PvpTokenModel;
import com.pandaos.pvpclient.models.PvpTokenModelCallback;
import com.pandaos.pvpclient.objects.PvpEntry;
import com.pandaos.pvpclient.objects.PvpLiveEntry;
import com.pandaos.pvpclient.objects.PvpLogin;
import com.pandaos.pvpclient.objects.PvpMeta;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends ActionBarActivity implements PvpEntryModelCallback, PvpTokenModelCallback, PvpLiveEntryModelCallback {

    private int currentPage = 1;
    private int selectedCategoryIndex = 0;
    private boolean loadMoreEntries = true;

    ActionBarDrawerToggle drawerToggle;

    @App
    BambooMobile application;

    @ViewById
    Toolbar main_toolbar;

    @ViewById
    DrawerLayout main_activity_drawer_layout;

    @ViewById
    SwipeRefreshLayout main_activity_refreshLayout;

    @ViewById
    ListView main_activity_main_listview;

    @ViewById
    ListView main_activity_drawer_listview;

    @ViewById
    TextView main_activity_drawer_visit_panda;

    @Bean
    CategoryListAdapter categoryListAdapter;

    @Bean
    EntryListAdapter entryListAdapter;

    @Bean
    PvpEntryModel entryModel;

    @Bean
    PvpLiveEntryModel liveEntryModel;

    @Bean
    PvpTokenModel tokenModel;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Click
    void main_activity_drawer_visit_panda() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.panda-os.com/mobile"));
        startActivity(browserIntent);
    }

    @OptionsItem(R.id.action_live_stream)
    void newLiveStream() {
        Intent startStreamActivity = new Intent(this, StreamActivity_.class);
        startStreamActivity.putExtra("streamUrl", "rtmp://stream.panda-os.com/pandaLive/0_4x6c2y8g");

        //We want to clear memory, so cleaning all running activities
        startStreamActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(startStreamActivity);
    }

    @AfterViews
    void afterViews() {
        setupToolBar();
        setupListViews();
        startRefresh();
        initEntriesList();
        hideStatusBar();

        PvpLogin user = new PvpLogin("email", "password");
        tokenModel.getToken(user, this);

//        application.getPiwikTracker().trackScreenView("/EntryListActivity");
    }

    void setupToolBar() {
        setSupportActionBar(main_toolbar);
        drawerToggle = new ActionBarDrawerToggle(this, main_activity_drawer_layout, main_toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getSupportActionBar().setTitle(categoryListAdapter.getItem(selectedCategoryIndex));
            }
        };
        main_activity_drawer_layout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getSupportActionBar().setTitle(categoryListAdapter.getItem(selectedCategoryIndex));
    }

    void setupListViews() {
        main_activity_drawer_listview.setAdapter(categoryListAdapter);
        ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(entryListAdapter);
        scaleAdapter.setAbsListView(main_activity_main_listview);
        main_activity_main_listview.setAdapter(scaleAdapter);

        main_activity_refreshLayout.setColorSchemeColors(Color.CYAN, Color.GREEN, Color.YELLOW, Color.MAGENTA);

        main_activity_refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initEntriesList();
            }
        });

        main_activity_main_listview.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (loadMoreEntries) {
                    currentPage++;
                    startRefresh();
                    getEntries();
                }
            }
        });

        main_activity_main_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PvpEntry entry = entryListAdapter.getItem(position);
                startPlayerActivity(entry);
            }
        });

        main_activity_drawer_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                main_activity_drawer_layout.closeDrawer(GravityCompat.START);
                selectedCategoryIndex = position;
                startRefresh();
                initEntriesList();
            }
        });
    }

    @Background
    void initEntriesList() {
        currentPage = 1;
        loadMoreEntries = true;
        getEntries();
    }

    @Background
    void getToken(String userId, String Password) {
        PvpLogin loginUser = new PvpLogin(userId, Password);
        tokenModel.getToken(loginUser, this);
    }

    @UiThread
    void startRefresh() {
        main_activity_refreshLayout.setRefreshing(true);
    }

    @UiThread
    void endRefresh() {
        main_activity_refreshLayout.setRefreshing(false);
    }

    @Background
    void getEntries() {
        entryModel.getEntriesByCategory(categoryListAdapter.getItem(selectedCategoryIndex), currentPage, this);
//        liveEntryModel.getLiveEntries(currentPage, this);
    }

    @Override
    public void entryRequestSuccess(PvpEntry entry) {

    }

    @Override
    public void entryRequestSuccess(List<PvpEntry> entries) {
        Log.i("Entry request success", "Ok");
        Log.i("List size", "" + entries.size());
        if (entries.size() < Constants.PAGE_SIZE) {
            loadMoreEntries = false;
        }

        endRefresh();
        if (currentPage == 1) {
            entryListAdapter.items.clear();
        }

        entryListAdapter.items.addAll(entries);
        updateEntryList();
    }

    @Override
    public void entryRequestFail() {
        endRefresh();
    }

    @UiThread
    void updateEntryList() {
        entryListAdapter.notifyDataSetChanged();
    }

    @UiThread(delay = 1000)
    void hideStatusBar() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        if (main_activity_drawer_layout.isDrawerOpen(GravityCompat.START)) {
            main_activity_drawer_layout.closeDrawers();
            return;
        }
        super.onBackPressed();
    }

    void startPlayerActivity(PvpEntry entry) {
        application.entryToPlay = entry;
        Intent intent = new Intent(this, EntryPlayerActivity_.class);
        startActivity(intent);
        overridePendingTransition(R.anim.trans_slide_in_left, R.anim.trans_slide_out_left);
    }

    @Override
    public void tokenRequestSuccess() {
    }

    @Override
    public void tokenRequestFail() {
        Log.i("Token", "Failed");
    }

    @Override
    public void liveEntryRequestSuccess(List<PvpLiveEntry> entries) {

    }

    @Override
    public void liveEntryCreateRequestSuccess(PvpLiveEntry entry) {

    }

    @Override
    public void liveEntryRequestFail() {

    }

    @Override
    public void entryRequestSuccessWithMeta(List<PvpEntry> entries, PvpMeta meta) {

    }
}
