package com.pandaos.bamboomobile.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pandaos.bamboomobile.util.BambooMobile;
import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.SharedPreferences_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oren Kosto on 5/2/15.
 */
@EBean
public class CategoryListAdapter extends BaseAdapter {
    List<String> items = new ArrayList<>();

    @App
    BambooMobile application;

    @Pref
    SharedPreferences_ prefs;

    @Bean
    PvpHelper pvpHelper;

    @RootContext
    Context context;

    @AfterInject
    void initAdapter() {

        if (items.size() > 0) {
            items.clear();
        }

        if (pvpHelper.getConfig() != null
                && pvpHelper.getConfig().home != null
                && pvpHelper.getConfig().home.get("categories") != null) {

            items = (List<String>)pvpHelper.getConfig().home.get("categories");
            items.add(0, "Featured");
        }
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        initAdapter();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CategoryListItemView listItemView = convertView == null
                ? CategoryListItemView_.build(context)
                : (CategoryListItemView) convertView;
        listItemView.bind(getItem(position));
        return listItemView;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public String getItem(int position) {
        if(items.size() > 0) {
            return items.get(position);
        } else {
            return "";
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
