package com.pandaos.bamboomobile.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kaltura.playersdk.KPPlayerConfig;
import com.kaltura.playersdk.PlayerViewController;
import com.kaltura.playersdk.RequestDataSource;
import com.kaltura.playersdk.events.KPEventListener;
import com.kaltura.playersdk.events.KPlayerState;
import com.kaltura.playersdk.types.KPError;
import com.pandaos.bamboomobile.R;
import com.pandaos.bamboomobile.util.AppUtils;
import com.pandaos.bamboomobile.util.BambooMobile;
import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.objects.PvpEntry;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.lang.reflect.Method;

@EActivity(R.layout.activity_entry_player)
public class EntryPlayerActivity extends ActionBarActivity {
    private static final String TAG = EntryPlayerActivity.class.getSimpleName();
    private static final int FULL_SCREEN_FLAG = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;

    private PvpEntry entry = null;
    private int defaultPlayerWidth = 0;
    private int defaultPlayerHeight = 0;
    private double currentPosition = 0;

    @App
    BambooMobile application;

    @Bean
    PvpHelper pvpHelper;

    @ViewById
    PlayerViewController activity_entry_player_player;

    @ViewById
    ImageView activity_entry_player_image;

    @ViewById
    TextView activity_entry_player_title;

    @ViewById
    TextView activity_entry_player_description;

    @ViewById
    Button activity_entry_player_share_button;

    @AfterViews
    void afterViews() {
        entry = application.entryToPlay;

        loadEntryImage();
        setupPlayer();
        fillEntryData();

//        application.getPiwikTracker().trackScreenView("/EntryPlayerActivity/" + entry.id);
//        application.getPiwikTracker().trackEvent("VodEntry", "playingEntry", entry.id);
        activity_entry_player_title.setSelected(true);


//        activity_entry_player_player.resumePlayer();
    }

    @Click
    void activity_entry_player_share_button() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, AppUtils.socialShareText(entry.id, entry.name));
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @UiThread
    void fillEntryData() {
        activity_entry_player_title.setText(entry.name);
        activity_entry_player_description.setText(entry.description);
    }

    @UiThread
    void loadEntryImage(){
        Picasso.with(this)
                .load(entry.thumbnailUrl + "/height/" + activity_entry_player_image.getHeight() + "/width/" + activity_entry_player_image.getWidth())
                .fit()
                .into(activity_entry_player_image);
    }

    void setupPlayer() {
        final ViewTreeObserver vto = activity_entry_player_player.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                activity_entry_player_player.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                defaultPlayerWidth = activity_entry_player_player.getMeasuredWidth();
                defaultPlayerHeight = activity_entry_player_player.getMeasuredHeight();
                showPlayerView();
            }
        });
        activity_entry_player_player.setActivity(this);

        activity_entry_player_player.addEventListener(new KPEventListener() {
            @Override
            public void onKPlayerStateChanged(PlayerViewController playerViewController, KPlayerState state) {

            }

            @Override
            public void onKPlayerError(PlayerViewController playerViewController, KPError error) {

            }

            @Override
            public void onKPlayerPlayheadUpdate(PlayerViewController playerViewController, float currentTime) {

            }

            @Override
            public void onKPlayerFullScreenToggeled(PlayerViewController playerViewController, boolean isFullscrenn) {
                setFullScreen();
            }
        });
//        activity_entry_player_player.setOnFullScreenListener(new OnToggleFullScreenListener() {
//
//            @Override
//            public void onToggleFullScreen() {
//                setFullScreen();
//
//            }
//        });
//        activity_entry_player_player.registerJsCallbackReady(new KPlayerJsCallbackReadyListener() {
//
//            @Override
//            public void jsCallbackReady() {
//                activity_entry_player_player.addKPlayerEventListener("doPlay", new KPlayerEventListener() {
//
//                    @Override
//                    public void onKPlayerEvent(Object body) {
//                        Log.d(TAG, "doPlay event called");
//                        if (getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT) {
//                            setFullScreen();
//                        }
//                    }
//
//                    @Override
//                    public String getCallbackName() {
//                        return "EventListenerDoPlay";
//                    }
//                });
//
//
//                activity_entry_player_player.addKPlayerEventListener("doPause", new KPlayerEventListener() {
//
//                    @Override
//                    public void onKPlayerEvent(Object body) {
//                        Log.d(TAG,"doPause event called");
//                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//                    }
//
//                    @Override
//                    public String getCallbackName() {
//                        return "EventListenerDoPause";
//                    }
//                });
//
//                activity_entry_player_player.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
//
//                    @Override
//                    public void onSystemUiVisibilityChange(int visibility) {
//                        Log.d(TAG, "onSystemVisibility change");
//                        if(visibility == FULL_SCREEN_FLAG) {
//                            Point size = getRealScreenSize();
//                            activity_entry_player_player.setPlayerViewDimensions(size.x, size.y);
//                        }else{
//                            Point size = getScreenWithoutNavigationSize();//getActivity().getWindowManager().getDefaultDisplay().getSize(size)
//                            activity_entry_player_player.setPlayerViewDimensions(size.x,size.y);
//                        }
//                    }
//                });
//
//            }
//
//        });

        String partnerId = (String) pvpHelper.getConfig().kaltura.get("partnerId");
        System.out.println("partner id: " + (String) pvpHelper.getConfig().kaltura.get("partnerId"));

        String uiconfId = (String) pvpHelper.getConfig().player.get("uiconfId");
        System.out.println("player uiconfid: " + (String) pvpHelper.getConfig().player.get("uiconfId"));

        String playerUrl = "http:" + ((String) pvpHelper.getConfig().player.get("playerUrl")).replace("mwEmbedLoader.php", "mwEmbedFrame.php");
        System.out.println("player url: " + playerUrl);

        System.out.println("liveEntry id: " + entry.id);

        KPPlayerConfig config = new KPPlayerConfig(playerUrl, uiconfId, partnerId).setEntryId(entry.id);
        activity_entry_player_player.initWithConfiguration(config);


//        activity_entry_player_player.setComponents(new RequestDataSource() {
//
//            @Override
//            public String getWid() {
//                System.out.println("partner id: " + (String) application.config.kaltura.get("partnerId"));
//                return (String) application.config.kaltura.get("partnerId");
//            }
//
//            @Override
//            public String getUrid() {
//                // TODO Auto-generated method stub
//                return null;
//            }
//
//            @Override
//            public boolean isSpecificVersionTemplate() {
//                return true;
//            }
//
//            @Override
//            public String getUiConfId() {
//                System.out.println("player uiconfid: " + (String) application.config.player.get("uiconfId"));
//                return (String) application.config.player.get("uiconfId");
//            }
//
//            @Override
//            public String getServerAddress() {
//                String playerUrl = ((String) application.config.player.get("playerUrl")).replace("mwEmbedLoader.php", "mwEmbedFrame.php");
//                System.out.println("player url: " + playerUrl);
//                return playerUrl;
//            }
//
//            @Override
//            public KPPlayerConfig getFlashVars() {
//                KPPlayerConfig playerConfig = new KPPlayerConfig("http://kgit.html5video.org/branches/master/mwEmbedFrame.php", "20540612", "243342");
//                return playerConfig;
//            }
//
//            @Override
//            public String getEntryId() {
//                System.out.println("liveEntry id: " + liveEntry.id);
//                return liveEntry.id;
//            }
//
//            @Override
//            public String getCacheStr() {
//                // TODO Auto-generated method stub
//                return null;
//            }
//        });
    }

    private void setPlayerFullScreen(){
        View decorView = getWindow().getDecorView(); //navigation view
        int uiOptions = FULL_SCREEN_FLAG;
        decorView.setSystemUiVisibility(uiOptions);
        Point size = getRealScreenSize();
        activity_entry_player_player.setPlayerViewDimensions(size.x, size.y);
    }

    private void setFullScreen(){
//        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            if(activity_entry_player_player.getHeight() > defaultPlayerHeight){
                Point size = getDefaultPlayerScreenSize();
                activity_entry_player_player.setPlayerViewDimensions(size.x, size.y);
            }else{
                setPlayerFullScreen();
            }
        } else {
            setPlayerFullScreen();
        }
    }

    private Point getDefaultPlayerScreenSize() {
        int height = defaultPlayerHeight;
        int width = defaultPlayerWidth;
        return new Point(width, height);
    }

    private Point getScreenWithoutNavigationSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics realMetrics = new DisplayMetrics();
        display.getMetrics(realMetrics);
        int width = realMetrics.widthPixels;
        int height = realMetrics.heightPixels;
        return new Point(width, height);
    }

    @SuppressLint("NewApi") private Point getRealScreenSize(){
        Display display = getWindowManager().getDefaultDisplay();
        int realWidth;
        int realHeight;

        if (Build.VERSION.SDK_INT >= 17){
            //new pleasant way to get real metrics
            DisplayMetrics realMetrics = new DisplayMetrics();
            display.getRealMetrics(realMetrics);
            realWidth = realMetrics.widthPixels;
            realHeight = realMetrics.heightPixels;

        } else {
            try {
                Method mGetRawH = Display.class.getMethod("getRawHeight");
                Method mGetRawW = Display.class.getMethod("getRawWidth");
                realWidth = (Integer) mGetRawW.invoke(display);
                realHeight = (Integer) mGetRawH.invoke(display);
            } catch (Exception e) {
                realWidth = display.getWidth();
                realHeight = display.getHeight();
                Log.e("Display Info", "Couldn't use reflection to get the real display metrics.");
            }

        }
        return new Point(realWidth,realHeight);
    }

    private void showPlayerView() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        activity_entry_player_player.setVisibility(RelativeLayout.VISIBLE);
//        Point size = new Point();
//        getActivity().getWindowManager().getDefaultDisplay().getSize(size);
        activity_entry_player_player.setPlayerViewDimensions( defaultPlayerWidth, defaultPlayerHeight, 0, 0 );
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if ( activity_entry_player_player.getVisibility() == RelativeLayout.VISIBLE ) {
            togglePlayerFullScreen();
        }
    }

    @UiThread(delay = 100)
    void togglePlayerFullScreen() {
        Point size;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            size = getDefaultPlayerScreenSize();
        } else {
            size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }

        activity_entry_player_player.setPlayerViewDimensions(size.x, size.y, 0, 0);
    }

    @Override
    public void onPause() {
        super.onPause();
//        application.getPiwikTracker().trackEvent("VodEntry", "stoppingEntry", entry.id);
        if ( activity_entry_player_player != null ) {
            activity_entry_player_player.releaseAndSavePosition();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( activity_entry_player_player != null ) {
            activity_entry_player_player.resumePlayer();
        }
    }
}
