package com.pandaos.bamboomobile.util;

import android.content.Context;
import android.hardware.Camera;
import android.media.AudioRecord;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import org.bytedeco.javacv.FFmpegFrameRecorder;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by elad on 24/04/2016.
 */
public class CameraView extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {

    private final String LOG_TAG = "Camera View";

    private SurfaceHolder mHolder;

    private PvpStreamModel_ streamModel;

    public void setCameraWithId(int cameraId) {
//        if (mCamera == camera) { return; }
        stopPreviewAndFreeCamera();

        streamModel.initCamera(cameraId);

        if (streamModel.camera != null) {
            List<Camera.Size> localSizes = streamModel.camera.getParameters().getSupportedPreviewSizes();
            requestLayout();

            try {
                streamModel.camera.setPreviewDisplay(mHolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            startPreview();
        }
    }

    /**
     * @param context
     * @param cameraId
     */
    public CameraView(Context context, int cameraId) {
        super(context);
        Log.w("camera", "camera view");

        streamModel = PvpStreamModel_.getInstance_(context);
        mHolder = getHolder();
        mHolder.addCallback(CameraView.this);
        setCameraWithId(cameraId);
//        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        streamModel.camera.setPreviewCallback(CameraView.this);
    }


    /**
     * @param holder
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            stopPreview();
            streamModel.camera.setPreviewDisplay(holder);
        } catch (IOException | NullPointerException exception) {
            stopPreviewAndFreeCamera();
        }
    }

    /**
     * @param holder
     * @param format
     * @param width
     * @param height
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (streamModel.camera == null) {return;}
        stopPreview();

        Camera.Parameters camParams = streamModel.camera.getParameters();
        List<Camera.Size> sizes = camParams.getSupportedPreviewSizes();
        // Sort the list in ascending order
        Collections.sort(sizes, new Comparator<Camera.Size>() {

            public int compare(final Camera.Size a, final Camera.Size b) {
                return a.width * a.height - b.width * b.height;
            }
        });

        // Pick the first preview size that is equal or bigger, or pick the last (biggest) option if we cannot
        // reach the initial settings of imageWidth/imageHeight.
        for (int i = 0; i < sizes.size(); i++) {
            if ((sizes.get(i).width >= PvpStreamModel.imageWidth && sizes.get(i).height >= PvpStreamModel.imageHeight) || i == sizes.size() - 1) {
                PvpStreamModel.imageWidth = sizes.get(i).width;
                PvpStreamModel.imageHeight = sizes.get(i).height;
                Log.v(LOG_TAG, "Changed to supported resolution: " + PvpStreamModel.imageWidth + "x" + PvpStreamModel.imageHeight);
                break;
            }
        }
        camParams.setPreviewSize(PvpStreamModel.imageWidth, PvpStreamModel.imageHeight);

        Log.v(LOG_TAG, "Setting imageWidth: " + PvpStreamModel.imageWidth + " imageHeight: " + PvpStreamModel.imageHeight + " frameRate: " + PvpStreamModel.frameRate);

        camParams.setPreviewFrameRate(PvpStreamModel.frameRate);
        Log.v(LOG_TAG, "Preview Framerate: " + camParams.getPreviewFrameRate());

        streamModel.camera.setParameters(camParams);

        // Set the holder (which might have changed) again
        try {
            streamModel.camera.setPreviewDisplay(holder);
            streamModel.camera.setPreviewCallback(CameraView.this);
            startPreview();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Could not set preview display in surfaceChanged");
        }
    }

    /**
     * @param holder
     */
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (streamModel.camera != null) {
//            streamModel.camera.stopPreview();
        }
    }

    /**
     * When this function returns, mCamera will be null.
     */
    public void stopPreviewAndFreeCamera() {
        try {
            stopPreview();
            if (streamModel.camera != null) {
                streamModel.camera.release();

                streamModel.camera = null;
            }
        } catch (RuntimeException e) {
            // The camera has probably just been released, ignore.
        }
    }

    /**
     *
     */
    public void startPreview() {
        if (!PvpStreamModel.isPreviewOn && streamModel.camera != null) {
            PvpStreamModel.isPreviewOn = true;
            streamModel.camera.setDisplayOrientation(90);
            streamModel.camera.startPreview();
        }
    }

    /**
     *
     */
    public void stopPreview() {
        if (PvpStreamModel.isPreviewOn && streamModel.camera != null) {
            PvpStreamModel.isPreviewOn = false;
            streamModel.camera.stopPreview();
        }
    }

    /**
     * @param data
     * @param camera
     */
    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (PvpStreamModel.audioRecord == null || PvpStreamModel.audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
            PvpStreamModel.startTime = System.currentTimeMillis();
            return;
        }
        if (PvpStreamModel.RECORD_LENGTH > 0) {
            int i = PvpStreamModel.imagesIndex++ % PvpStreamModel.images.length;
            PvpStreamModel.yuvImage = PvpStreamModel.images[i];
            PvpStreamModel.timestamps[i] = 1000 * (System.currentTimeMillis() - PvpStreamModel.startTime);
        }
            /* get video data */
        if (PvpStreamModel.yuvImage != null && PvpStreamModel.recording) {
            ((ByteBuffer) PvpStreamModel.yuvImage.image[0].position(0)).put(data);

            if (PvpStreamModel.RECORD_LENGTH <= 0) try {
                Log.v(LOG_TAG, "Writing Frame");
                long t = 1000 * (System.currentTimeMillis() - PvpStreamModel.startTime);
                if (t > PvpStreamModel.recorder.getTimestamp()) {
                    PvpStreamModel.recorder.setTimestamp(t);
                }
                if(PvpStreamModel.yuvImage != null) {
                    PvpStreamModel.recorder.record(PvpStreamModel.yuvImage);
                }
            } catch (FFmpegFrameRecorder.Exception e) {
                Log.v(LOG_TAG, e.getMessage());
                e.printStackTrace();
            }
        }
    }
}