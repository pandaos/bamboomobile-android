package com.pandaos.bamboomobile.util;

/**
 * Created by Oren Kosto on 4/24/15.
 */
public class Constants {
    public static final String PVP_SERVER_BASE_URL = "http://demo.bamboo-video.com";
    public static final String PVP_SERVER_API_PREFIX = "/api";
    public static final String PVP_SERVER_API_SUFFIX = "?format=json";
    public static final int PVP_SERVER_COOKIE_EXPIRY = 1209600;
    public static final boolean PVP_SERVER_DEBUG_ENABLED = false;

    public static final int PAGE_SIZE = 20;
    public static final int MAX_PAGES = 100;

    public static final boolean SYSTEM_UI_AUTO_HIDE = true;
    public static final int SYSTEM_UI_AUTO_HIDE_DELAY_MILLIS = 3000;
    public static final int SYSTEM_UI_HIDER_FLAGS = 0;
}
