package com.pandaos.bamboomobile.util;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.pandaos.pvpclient.objects.PvpEntry;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EApplication;
import org.piwik.sdk.Tracker;

/**
 * Created by Oren Kosto on 4/27/15.
 */
@EApplication
public class BambooMobile extends MultiDexApplication {

//    public List<PvpEntry> entries = new ArrayList<>();

    public PvpEntry entryToPlay = null;

    public String piwikBaseUrl = "";
    public int piwikSiteId = 1;
    public int piwikDispatchInterval = 0;
    Tracker piwikTracker;

    public void onCreate() {
        super.onCreate();
        init();
    }

    @Background
    void init() {

    }

//    public synchronized Tracker getPiwikTracker() {
//        if (piwikTracker != null) {
//            return piwikTracker;
//        }
//
//        try {
//            piwikTracker = Piwik.getInstance(this).newTracker(piwikBaseUrl + "/piwik", piwikSiteId);
//            piwikTracker.setDispatchInterval(piwikDispatchInterval);
//        } catch (MalformedURLException e) {
//            Log.w("Bamboo Piwik Tracker", "url is malformed", e);
//            return null;
//        }
//
//        return piwikTracker;
//    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
