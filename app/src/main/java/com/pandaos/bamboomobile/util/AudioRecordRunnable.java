package com.pandaos.bamboomobile.util;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.bytedeco.javacv.FFmpegFrameRecorder;

import java.nio.ShortBuffer;

/**
 * Created by elad on 24/04/2016.
 */
@EBean
public class AudioRecordRunnable implements Runnable {

    private final String LOG_TAG = "Audio Record";

    @Bean
    PvpStreamModel pvpStreamModel;

    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

        // Audio
        int bufferSize;
        ShortBuffer audioData = null;
        int bufferReadResult;

        bufferSize = AudioRecord.getMinBufferSize(PvpStreamModel.sampleAudioRateInHz,
                AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        PvpStreamModel.audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, PvpStreamModel.sampleAudioRateInHz,
                AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);

        if (PvpStreamModel.RECORD_LENGTH > 0) {
            PvpStreamModel.samplesIndex = 0;
            PvpStreamModel.samples = new ShortBuffer[PvpStreamModel.RECORD_LENGTH * PvpStreamModel.sampleAudioRateInHz * 2 / bufferSize + 1];
            for (int i = 0; i < PvpStreamModel.samples.length; i++) {
                PvpStreamModel.samples[i] = ShortBuffer.allocate(bufferSize);
            }
        } else {
            audioData = ShortBuffer.allocate(bufferSize);
        }

        Log.d(LOG_TAG, "audioRecord.startRecording()");
        PvpStreamModel.audioRecord.startRecording();

        /* ffmpeg_audio encoding loop */
        while (PvpStreamModel.runAudioThread) {
            if (PvpStreamModel.RECORD_LENGTH > 0) {
                audioData = PvpStreamModel.samples[PvpStreamModel.samplesIndex++ % PvpStreamModel.samples.length];
                audioData.position(0).limit(0);
            }

            //Log.v(LOG_TAG,"recording? " + recording);
            bufferReadResult = PvpStreamModel.audioRecord.read(audioData.array(), 0, audioData.capacity());
            audioData.limit(bufferReadResult);
            if (bufferReadResult > 0) {
                Log.v(LOG_TAG, "bufferReadResult: " + bufferReadResult);
                // If "recording" isn't true when start this thread, it never get's set according to this if statement...!!!
                // Why?  Good question...
                if (PvpStreamModel.recording) {
                    if (PvpStreamModel.RECORD_LENGTH <= 0) try {
                        PvpStreamModel.recorder.recordSamples(audioData);
                        //Log.v(LOG_TAG,"recording " + 1024*i + " to " + 1024*i+1024);
                    } catch (FFmpegFrameRecorder.Exception e) {
                        Log.v(LOG_TAG,e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        }
        Log.v(LOG_TAG,"AudioThread Finished, release audioRecord");

            /* encoding finish, release recorder */
        if (PvpStreamModel.audioRecord != null) {
            PvpStreamModel.audioRecord.stop();
            PvpStreamModel.audioRecord.release();
            PvpStreamModel.audioRecord = null;
            Log.v(LOG_TAG,"audioRecord released");
        }
    }
}