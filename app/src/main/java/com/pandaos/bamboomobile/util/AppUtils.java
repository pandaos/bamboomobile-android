package com.pandaos.bamboomobile.util;

/**
 * Created by orenkosto on 7/20/15.
 */
public class AppUtils {

    private static final String socialShareInitialText = "I'm watching <entry-name> on Bamboo Mobile! Check it out! <player-url>";

    public static String socialShareText(String entryId, String entryName) {
        String socialShareFinalText = AppUtils.socialShareInitialText.replace("<entry-name>", entryName);
        socialShareFinalText = socialShareFinalText.replace("<user-name>", entryName);
        socialShareFinalText = socialShareFinalText.replace("<player-url>", AppUtils.playPageUrl(entryId, entryName));

        return socialShareFinalText;
    }

    public static String playPageUrl(String entryId, String entryName) {
        return Constants.PVP_SERVER_BASE_URL + "/play//" + entryName + "/" + entryId;
    }
}
