package com.pandaos.bamboomobile.util;

import android.hardware.Camera;
import android.media.AudioRecord;
import android.util.Log;

import org.androidannotations.annotations.EBean;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;

import java.nio.ShortBuffer;

@EBean(scope = EBean.Scope.Singleton)
public class PvpStreamModel {

    public final static String CLASS_LABEL = "RecordActivity";
    public final static String LOG_TAG = CLASS_LABEL;

    public static String ffmpeg_link = "";

    public static long startTime = 0;
    public static boolean recording = false;

    public static FFmpegFrameRecorder recorder;
    public static boolean isPreviewOn = false;

    public static int sampleAudioRateInHz = 44100;
    public static int imageWidth = 640;
    public static int imageHeight = 480;
    public static int frameRate = 30;

    /* audio data getting thread */
    public static AudioRecord audioRecord;
    public static AudioRecordRunnable audioRecordRunnable;
    public static Thread audioThread;
    public static volatile boolean runAudioThread = true;

    public static Frame yuvImage = null;

    /* The number of seconds in the continuous record loop (or 0 to disable loop). */
    public static final int RECORD_LENGTH = 0;
    public static Frame[] images;
    public static long[] timestamps;
    public static ShortBuffer[] samples;
    public static int imagesIndex, samplesIndex;

    public Camera camera;
    private int cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;

    //---------------------------------------
    // initialize ffmpeg_recorder
    //---------------------------------------
    private void initRecorder() {

        Log.w(LOG_TAG, "init recorder");

        if (RECORD_LENGTH > 0) {
            imagesIndex = 0;
            images = new Frame[RECORD_LENGTH * frameRate];
            timestamps = new long[images.length];
            for (int i = 0; i < images.length; i++) {
                images[i] = new Frame(imageWidth, imageHeight, Frame.DEPTH_UBYTE, 2);
                timestamps[i] = -1;
            }
        } else if (yuvImage == null) {
            yuvImage = new Frame(imageWidth, imageHeight, Frame.DEPTH_UBYTE, 2);
            Log.i(LOG_TAG, "create yuvImage");
        }

        Log.i(LOG_TAG, "ffmpeg_url: " + ffmpeg_link);
        recorder = new FFmpegFrameRecorder(ffmpeg_link, imageWidth, imageHeight, 1);
        recorder.setFormat("flv");
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
//        recorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);
        recorder.setSampleRate(sampleAudioRateInHz);
        // Set in the surface changed method
        recorder.setFrameRate(frameRate);

        Log.i(LOG_TAG, "recorder initialize success");

        audioRecordRunnable = new AudioRecordRunnable();
        audioThread = new Thread(audioRecordRunnable);
        runAudioThread = true;
    }

    /**
     * Start recording live stream
     */
    public void startRecording() {
        initRecorder();

        try {
            recorder.start();
            startTime = System.currentTimeMillis();
            recording = true;
            audioThread.start();

        } catch (FFmpegFrameRecorder.Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stop recording live stream
     */
    public void stopRecording() {
        runAudioThread = false;

        try {
            audioThread.join();
        } catch (InterruptedException e) {
            // reset interrupt to be nice
            Thread.currentThread().interrupt();
            return;
        }
        audioRecordRunnable = null;
        audioThread = null;

        if (recorder != null && recording) {
            if (RECORD_LENGTH > 0) {
                Log.v(LOG_TAG,"Writing frames");
                try {
                    int firstIndex = imagesIndex % samples.length;
                    int lastIndex = (imagesIndex - 1) % images.length;
                    if (imagesIndex <= images.length) {
                        firstIndex = 0;
                        lastIndex = imagesIndex - 1;
                    }
                    if ((startTime = timestamps[lastIndex] - RECORD_LENGTH * 1000000L) < 0) {
                        startTime = 0;
                    }
                    if (lastIndex < firstIndex) {
                        lastIndex += images.length;
                    }
                    for (int i = firstIndex; i <= lastIndex; i++) {
                        long t = timestamps[i % timestamps.length] - startTime;
                        if (t >= 0) {
                            if (t > recorder.getTimestamp()) {
                                recorder.setTimestamp(t);
                            } else {
                                Log.d("Time Stamp", "Incorrect timestamp: " + t + " when Recorder on: " + recorder.getTimestamp());
                            }
                            recorder.record(images[i % images.length]);
                        }
                    }

                    firstIndex = samplesIndex % samples.length;
                    lastIndex = (samplesIndex - 1) % samples.length;
                    if (samplesIndex <= samples.length) {
                        firstIndex = 0;
                        lastIndex = samplesIndex - 1;
                    }
                    if (lastIndex < firstIndex) {
                        lastIndex += samples.length;
                    }
                    for (int i = firstIndex; i <= lastIndex; i++) {
                        recorder.recordSamples(samples[i % samples.length]);
                    }
                } catch (FFmpegFrameRecorder.Exception e) {
                    Log.v(LOG_TAG,e.getMessage());
                    e.printStackTrace();
                }
            }

            recording = false;
            Log.v(LOG_TAG,"Finishing recording, calling stop and release on recorder");
            try {
                recorder.stop();
                recorder.release();
            } catch (FFmpegFrameRecorder.Exception e) {
                e.printStackTrace();
            }
            recorder = null;
        }
    }

    public void initCamera() {
        this.camera = Camera.open(this.cameraId);
    }

    public void initCamera(int cameraId) {
        this.cameraId = cameraId;
        initCamera();
    }

}